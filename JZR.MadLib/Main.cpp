/*
	Lab Exercise #3 - Mad Lib
	Jason Ryan
	2020-03-06
*/

#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

string GetResult(vector<string> data)
{
	return
		"My Ideal President\n\n"
		"There are "  + data[0] + " qualities I admire in a leader.\n"
		"First, he or she must be "  + data[1] + ", because "  + data[2] + " the "  + data[3] + " is a sign of "  + data[4] + " character.\n"
		"Second, a good President should be "  + data[5] + ", always making sure people feel "  + data[6] + " and "  + data[7] + ".\n"
		"Third, a President represents our country around the "  + data[8] + ", and should be "  + data[9] + " when "  + data[10] + " with other world "  + data[11] + ".\n"
		"Strength of "  + data[12] + ", a good sense of "  + data[13] + ", and "  + data[14] + " leadership are all things my President would have.";
}

int main()
{
	vector<string> fillers =
	{
		"a number",
		"an adjective",
		"a gerund (verb-ing)",
		"a noun",
		"an adjective",
		"an adjective",
		"an adjective",
		"an adjective",
		"a place",
		"an adjective",
		"a gerund (verb-ing)",
		"a plural noun",
		"a noun",
		"a noun",
		"an adjective",

	};

	vector<string> userData;

	for (int i = 0; i < fillers.size(); i++)
	{
		cout << "Enter " << fillers[i] << ": ";
		string data;
		getline(cin,data);
		userData.push_back(data);
	}

	string result = GetResult(userData);
	cout << "\n\n" << result << "\n\n";

	char ans;
	cout << "Do you want to save the result to a text file? (y/n): ";
	cin >> ans;
	
	if (ans == 'Y' || ans == 'y')
	{
		cin.ignore();
		cout << "Where do you want to save the file to? (path): ";
		string path;
		getline(cin,path);

		ofstream ofs(path);
		ofs << result;
		ofs.close();
		cout << "\nFile has been saved to " << path;
	}
	cout << "\n\nPress any key to exit.";
	_getch();
	return 0;
}